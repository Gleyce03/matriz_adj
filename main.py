from matriz_adj import MatrizAdjacente

arquivo = open("texto.txt", 'r')
primeira_linha = arquivo.readline()

arestas = []
vertices = []

for aresta in arquivo:
    aresta1 = aresta.split(",")[0].strip()
    aresta2 = aresta.split(",")[1].strip()

    arestas.append((aresta1, aresta2))

    if aresta1 not in vertices:
        vertices.append(aresta1)
    
    if aresta2 not in vertices:
        vertices.append(aresta2)

arquivo.close()
print(primeira_linha)
print('Arestas: ', arestas)
print('Vértices: ', vertices)

grafo = MatrizAdjacente(vertices, arestas, primeira_linha)
grafo.print_matriz()

print("Insira uma das opções:")
print("1 - Insira dois vértices para saber se são ou não adjacentes")
print("2 - Insira um vértice para saber o seu grau")
print("3 - Insira um vértice para saber seus vizinhos")
print("4 - Visitar todas as arestas do grafo")
print("5 - SAIR")

valor_inserido = 0
while(valor_inserido != 5):

    print_opcao = "\nInsira uma nova opção"
    valor_inserido = int(input())

    if valor_inserido == 1:
        print("Insira dois vértices para saber se são ou não adjacentes")
        vx = (input("Informe o vértice vX: "))
        vy = (input("Informe o vértice vY: "))
        if self.vertices:
            print("São adjacentes")
        else:
            print("Não são adjacentes")   

        print(print_opcao)
    
    elif valor_inserido == 2:
        vertice = (input('Insira um vértice para saber o seu grau: '))
        print(vertice)
        
        print(print_opcao)
    
    elif valor_inserido == 3:
        vertice = (input('Insira um vértice para saber seus vizinhos: '))
        print(vertice)

        print(print_opcao)
    
    elif valor_inserido == 4:
        print('Arestas do grafo:')
        print(arestas)

        print(print_opcao)

    elif valor_inserido == 5:
        print('Até mais!')

    else:
        print('Opção inválida!')
        print(print_opcao)