import string

class MatrizAdjacente:

    def __init__(self, vertices, arestas, direcao):
        self.vertices = len(vertices)
        self.adj = [[0] * self.vertices for _ in range(self.vertices)]
        self.direcao = direcao
        self.adiciona_arestas(vertices, arestas, direcao)
    
    def adiciona_arestas(self, vertices, arestas, direcao):

        for u, v in arestas:
            if self.numero(u) and self.numero(v):
                self.adj[u - 1][v - 1] += 1
                if direcao.strip() == 'ND':
                    self.adj[v - 1][u - 1] = 1
            else:
                for i in vertices:
                    if i == u:
                       valor_u = vertices.index(i)
                    else:
                        if i == v:
                            valor_v = vertices.index(i)

                self.adj[valor_u][valor_v] = 1
                if direcao.strip() == 'ND':
                    self.adj[valor_v][valor_u] = 1

    def print_matriz(self):
        print('A matriz de adjacência é: ')
        for _ in range(self.vertices):
            print(self.adj[_])


    def numero(self, valor):
        try:
            int(valor)
        except ValueError:
            return False
        return True